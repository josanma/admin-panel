import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PanelComponent } from './panel.component';
import { IncomeComponent } from './income/income.component';
import { DetailComponent } from './detail/detail.component';
import { StatisticsComponent } from './statistics/statistics.component';


const childRoutes: Routes = [
  {
    path: '',
    component: StatisticsComponent
  },
  {
    path: 'details',
    component: DetailComponent
  },
  {
    path: 'income',
    component: IncomeComponent
  }
];

const routes: Routes = [
  {
    path: '',
    component: PanelComponent,
    children: childRoutes
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanelRoutingModule {
}
